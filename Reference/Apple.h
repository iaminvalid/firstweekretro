#pragma once
#include "GameObject.h"

class Apple : public GameObject
{
public:
	Apple() = delete;
	Apple(aie::Texture* m_texture, int width, int height);
	~Apple() = default;

	void Update(float deltaTime) override;

	void SetFallSpeed(float speed) { m_fallSpeed = speed; }
	void SetRandomFallSpeed(float min, float max);
	float GetFallSpeed() const { return m_fallSpeed; }

private:
	float m_fallSpeed;
};