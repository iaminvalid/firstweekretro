#pragma once
#include "GameObject.h"

class Basket : public GameObject
{
public:
	Basket() = delete;
	Basket(aie::Texture* m_texture, int width, int height);
	~Basket() = default;

	void Update(float deltaTime) override;

	void SetSpeed(float speed) { m_speed = speed; }
	float GetSpeed() const { return m_speed; }
private:
	float m_speed;
};