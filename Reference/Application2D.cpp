#include "Application2D.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <random>

#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "Basket.h"
#include "Apple.h"

Application2D::Application2D() {
	m_timeBetweenApples = 2.0f;
	m_timeForNextApple = m_timeBetweenApples;
}

Application2D::~Application2D() {

}

bool Application2D::startup() {
	
	m_2dRenderer = new aie::Renderer2D();

	m_font = new aie::Font("./font/consolas_bold.ttf", 48);

	m_backgroundTexture = new aie::Texture("./textures/gamebackground.png");
	m_basketTexture = new aie::Texture("./textures/basket.png");
	m_appleTexture = new aie::Texture("./textures/apple.png");

	m_basket = new Basket(m_basketTexture, 256, 128);
	m_basket->SetSpeed(500.0f);
	m_basket->SetPosition(glm::vec3(
		getWindowWidth() * 0.5f, 50, -1)
	);

	m_currentScore = 0;

	m_cameraX = 0;
	m_cameraY = 0;
	m_timer = 0;

	return true;
}

void Application2D::shutdown() {
	
	delete m_basket;

	delete m_appleTexture;
	delete m_basketTexture;
	
	
	delete m_2dRenderer;
}

void Application2D::update(float deltaTime) {

	m_timer += deltaTime;

	GameUpdate(deltaTime);

	// input example
	aie::Input* input = aie::Input::getInstance();
	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
	{
		int i = 3;
	}
}

void Application2D::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// set the camera position before we begin rendering
	m_2dRenderer->setCameraPos(m_cameraX, m_cameraY);

	// begin drawing sprites
	m_2dRenderer->begin();

	auto screenWidth = getWindowWidth();
	auto screenHeight = getWindowHeight();
	m_2dRenderer->drawSprite(m_backgroundTexture, 0, 0, (float)screenWidth, (float)screenHeight, 0, 0, 0, 0);

	GameRender();

	// done drawing sprites
	m_2dRenderer->end();
}

void Application2D::GameUpdate(float deltaTime)
{
	m_basket->Update(deltaTime);

	if (m_timer > m_timeForNextApple)
	{
		CreateApple();
		m_timeForNextApple = m_timer + m_timeBetweenApples;
	}

	for (auto it = m_apples.begin(); it != m_apples.end(); ++it)
	{
		Apple* apple = *it;
		apple->Update(deltaTime);
	}
	CheckForCollectedApples();
	RemoveOffScreenApples();

	/*
	for(auto& apple : m_apples)
	{

	}
	*/
}

void Application2D::GameRender()
{
	m_basket->Render(m_2dRenderer);

	for (auto it = m_apples.begin(); it != m_apples.end(); ++it)
	{
		Apple* apple = *it;
		apple->Render(m_2dRenderer);
	}

	char scoreText[128];
	sprintf_s(scoreText, "Score: %i", m_currentScore);

	m_2dRenderer->drawText(m_font, scoreText, 20, 0);
}

void Application2D::CreateApple()
{
	auto* apple = new Apple(m_appleTexture, 64, 64);
	apple->SetRandomFallSpeed(100, 300);

	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_real_distribution<float> dist(0.0f, (float)getWindowWidth());

	apple->SetPosition(glm::vec3(
		dist(mt), getWindowHeight() + 64, -0.5f)
	);

	m_apples.push_back(apple);
}

void Application2D::CheckForCollectedApples()
{
	for (auto it = m_apples.begin(); it != m_apples.end();)
	{
		const Apple* apple = *it;
		if (apple->IsColliding(*m_basket))
		{
			m_currentScore += (int)apple->GetFallSpeed();

			delete apple;
			it = m_apples.erase(it);
		}

		if (it != m_apples.end()) ++it;
	}
}

void Application2D::RemoveOffScreenApples()
{
	for (auto it = m_apples.begin(); it != m_apples.end();)
	{
		const Apple* apple = *it;
		if (apple->GetPosition().y < -64.0f)
		{
			delete apple;
			it = m_apples.erase(it);
		}

		if (it != m_apples.end()) ++it;
	}
}
