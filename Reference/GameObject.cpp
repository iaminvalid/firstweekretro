#include "GameObject.h"

#include "Texture.h"
#include "Renderer2D.h"

GameObject::GameObject(aie::Texture* texture, int width, int height)
	: m_texture(texture)
	, m_position(0.0f, 0.0f, 0.0f)
	, m_width(width)
	, m_height(height)
{
}

void GameObject::Render(aie::Renderer2D * renderer)
{
	renderer->drawSprite(m_texture, m_position.x, m_position.y, (float)m_width, (float)m_height, 0, m_position.z);
}

bool GameObject::CheckCollision(const GameObject & first, const GameObject & second)
{
	if (first.m_bounds.topLeft.x > second.m_bounds.bottomRight.x) return false;
	if (first.m_bounds.bottomRight.x < second.m_bounds.topLeft.x) return false;
	if (first.m_bounds.topLeft.y < second.m_bounds.bottomRight.y) return false;
	if (first.m_bounds.bottomRight.y > second.m_bounds.topLeft.y) return false;

	return true;
}

void GameObject::UpdateBounds()
{
	m_bounds.topLeft = glm::vec2(
		m_position.x - (m_width * 0.5f),
		m_position.y + (m_height * 0.5f)
	);
	m_bounds.bottomRight = glm::vec2(
		m_position.x + (m_width * 0.5f),
		m_position.y - (m_height * 0.5f)
	);
}



