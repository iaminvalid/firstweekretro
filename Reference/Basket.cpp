#include "Basket.h"
#include "Input.h"

Basket::Basket(aie::Texture * m_texture, int width, int height)
	: GameObject(m_texture, width, height)
	, m_speed(100.0f)
{

}

void Basket::Update(float deltatime)
{
	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_KEY_A))
	{
		m_position.x -= m_speed * deltatime;
	}
	if (input->isKeyDown(aie::INPUT_KEY_D))
	{
		m_position.x += m_speed * deltatime;
	}

	UpdateBounds();
}
