#include "Apple.h"
#include <random>

Apple::Apple(aie::Texture * m_texture, int width, int height)
	: GameObject(m_texture, width, height)
{
}

void Apple::Update(float deltaTime)
{
	m_position.y -= m_fallSpeed * deltaTime;

	UpdateBounds();
}

void Apple::SetRandomFallSpeed(float min, float max)
{
	static std::random_device rd;
	static std::mt19937 mt(rd());
	std::uniform_real_distribution<float> dist(min, max);

	m_fallSpeed = dist(mt);
}
