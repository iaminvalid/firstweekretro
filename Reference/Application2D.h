#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "Audio.h"

#include <list>

class Basket;
class Apple;

class Application2D : public aie::Application {
public:

	Application2D();
	virtual ~Application2D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	void GameUpdate(float deltaTime);
	void GameRender();

	void CreateApple();
	void CheckForCollectedApples();
	void RemoveOffScreenApples();

protected:
	aie::Renderer2D*	m_2dRenderer;
	

	aie::Font*			m_font;
	aie::Texture*		m_backgroundTexture;
	aie::Texture*		m_basketTexture;
	aie::Texture*		m_appleTexture;

	Basket*				m_basket;
	std::list<Apple*>	m_apples;

	float m_cameraX, m_cameraY;
	float m_timer;

	float m_timeForNextApple;
	float m_timeBetweenApples;

	int  m_currentScore;
};