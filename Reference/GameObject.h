#pragma once
#include <glm/glm.hpp>
#include <glm/ext.hpp>

namespace aie
{
	class Texture;
	class Renderer2D;
}

class GameObject
{
private:
	struct Bounds
	{
		glm::vec2 topLeft;
		glm::vec2 bottomRight;
	};

public:
	GameObject() = delete;
	GameObject(aie::Texture* texture, int width, int height);
	virtual ~GameObject() = default;

	virtual void	Update(float deltaTime) = 0;
	void			Render(aie::Renderer2D* renderer);

	void			SetPosition(const glm::vec3& pos) { m_position = pos; }
	glm::vec3		GetPosition() const { return m_position; }

	const aie::Texture* GetTexture() const { return m_texture; }
	int					GetWidth() const { return m_width; }
	int					GetHeight() const { return m_height; }


	//GameObject::CheckCollision(m_basket, apple);
	static bool CheckCollision(const GameObject& first, const GameObject& second);
	bool IsColliding(const GameObject& other) const { return CheckCollision(*this, other); }
protected:
	void UpdateBounds();
	
	glm::vec3 m_position;

private:
	aie::Texture* m_texture;


	Bounds m_bounds;
	int m_width;
	int m_height;
};
