#include "Enemy.h"
#include "EnemySpawner.h"
#include <random>
Enemy::Enemy()
{
	//sets default stats
	Curpos.PosX = 400;
	Curpos.PosY = 400;
	isAlive = true;
	MoveSpeed = 1000;
	CanShoot = false;

}
Enemy::Enemy(aie::Texture * a_texture, int a_width, int a_height, float a_x, float a_y, EnemySpawn * a_enemySpawn)
{
}
Enemy::Enemy(float a_x, float a_y, EnemySpawn* a_Parent, float a_Width, float a_Height, Player* a_Player )
{
	//sets customizable stats, speed, x + y position
	Curpos.PosX = a_x;
	Curpos.PosY = a_y;
	Width = a_Width;
	Height = a_Height;
	isAlive = true;
	MoveSpeed = 200;
	parent = a_Parent;
	CanShoot = false;
	TargetPlayer = a_Player;

	
}

Enemy::~Enemy()
{
}

void Enemy::Draw(aie::Renderer2D* a_2drenderer)
{
	//gets things on screen
	//m_2dRenderer->setUVRect(0, 0, 1, 1);
	//m_2dRenderer->drawSprite(m_texture, 600, 400, 0, 0, 0, 1);
	a_2drenderer->drawSprite(m_Texture, Curpos.PosX, Curpos.PosY, Width, Height);
}

void Enemy::update(float DeltaTime)
{

	//changes Xposition by movespeed (relative to real time)
	Curpos.PosX += MoveSpeed * DeltaTime;
	if (Curpos.PosX >= 1180 || Curpos.PosX <= 100)
	{
		if (parent->CanBounce() == false)
		{
			parent->HasBounced();
		}
	}

		
	UpdateBounds();
	
	if (Curpos.PosY <= 100)
	{
		//checks if enemy is in loss condition. 
		parent->HasInvaded = true;
	}
	if (CanShoot)
	{
		if (rand() % 100 + 1 == 54)
		{
		//	isAlive = false;
			Shoot();
		}
	}
}


void Enemy::Shoot()
{
	
		//Create bullet here
	
	//shootTimer = rand() % 6 + 1;
	//
	//	isAlive = false;
	
	

	

}

void Enemy::invertSpeed()
{
	MoveSpeed *= -1;
	Curpos.PosY -= 50;
}

void Enemy::UpdateBounds()
{

	m_Bounds.TopLeft.PosX = Curpos.PosX - (Width * 0.5f);
	m_Bounds.TopLeft.PosY = Curpos.PosY + (Height * 0.5f);

	m_Bounds.BottomRight.PosX = Curpos.PosX + (Width * 0.5f);
	m_Bounds.BottomRight.PosY = Curpos.PosY - (Height * 0.5f);
}


