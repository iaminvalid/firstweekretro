# include "Bullet.h"
#include "Application2D.h"

Bullet::Bullet()
{

};

Bullet::Bullet(Player* parentIsPlayer, EnemySpawn* enemies)
{
	direction = +3;
	BulletPos.Xpos = parentIsPlayer->PlayerPos.Xpos + parentIsPlayer->Width / 2;
	BulletPos.Ypos = parentIsPlayer->PlayerPos.Ypos + parentIsPlayer->Height + 50;

	ParentP = parentIsPlayer;
	ParentE = nullptr;
	Target = nullptr;
	Targets = enemies;

	BulletTexture = PBullet;
}

Bullet::Bullet(Enemy* parentIsEnemy, Player* PlayerTarget)
{
	direction = -3;
	BulletPos.Xpos = parentIsEnemy->Curpos.PosX + parentIsEnemy->Width / 2;
	BulletPos.Ypos = parentIsEnemy->Curpos.PosY + parentIsEnemy->Height - 50;

	ParentP = nullptr;
	ParentE = parentIsEnemy;
	Target = PlayerTarget;
	Targets = nullptr;

	BulletTexture = EBullet;
}

Bullet::~Bullet()
{

}

void Bullet::Update(float deltaTime)
{
	//move bullet
	BulletPos.Ypos += 200 * deltaTime;
	updateBounds();
	//if parent is player, check for collision with enemies
	if (ParentP != nullptr && !HasHit)
	{
		for (int i = 0; i < Targets->total; i++)
		{
			if (Targets->TotalEnemies[i] != nullptr)
			{

				HasHit = BulletCollider.IsEnemyColliding(Targets->TotalEnemies[i], this);
				if (HasHit)
				{
					Targets->TotalEnemies[i]->isAlive = false;
					this->ParentP->BulletFired = false;
					this->ParentP->Score += 20;
					break;
				}
			}
		}
	}
	// if parent is enemy, check for collision with player
	else if (ParentE != nullptr)
	{

		HasHit = BulletCollider.IsPlayerColliding(Target, this);
		if (HasHit)
		{
			Target->IsAlive = false;
		}	
	}
	//else if bullet is at top/ bottom of screen
	 if (BulletPos.Ypos > 721 + Height || BulletPos.Ypos < -1)
	{

		this->ParentP->BulletFired = false;
	}

}

void Bullet::Draw(aie::Renderer2D* a_renderer2D)
{
	float X = BulletPos.Xpos;
	float Y = BulletPos.Ypos;

	a_renderer2D->drawSprite(BulletTexture, X, Y, Width, Height,90.0f);
}

void Bullet::updateBounds()
{

	m_Bounds.TopLeft.Xpos = BulletPos.Xpos - (Width * 0.5f);
	m_Bounds.TopLeft.Ypos = BulletPos.Ypos + (Height * 0.5f);

	m_Bounds.BottomRight.Xpos = BulletPos.Xpos + (Width * 0.5f);
	m_Bounds.BottomRight.Ypos = BulletPos.Ypos - (Height * 0.5f);
}