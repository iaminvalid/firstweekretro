#include "Shield.h"
////////////////////////////////////////////////////////////////////////////////////
//Shield class
///////////////////////////////////////////////////////////////////////////////////
Shield::Shield()
{

}

Shield::Shield( float X, float Y, aie::Renderer2D* a_Renderer, aie::Texture* a_Texture)
{
	Position.Xpos = X;
	Position.Ypos = Y;

	Size.width =  25;
	Size.Height = 25;

	m_Renderer = a_Renderer;
	m_Texture = a_Texture;
}

Shield::~Shield()
{

}

void Shield::Draw()
{

	float xx = Position.Xpos;
	float xY = Position.Ypos;
	float xW = Size.width;
	float xH = Size.Height;
	m_Renderer->drawBox(Position.Xpos, Position.Ypos, Size.width, Size.Height);
}

/////////////////////////////////////////////////////////////////////////////////////
//Shield Generator Class
////////////////////////////////////////////////////////////////////////////////////
ShieldGenerator::ShieldGenerator()
{

}

ShieldGenerator::ShieldGenerator(int a_sectionSize, aie::Renderer2D* a_Renderer, aie::Texture* a_Texture)
{
	SectionSize = a_sectionSize;
	SectionA = new Shield*[a_sectionSize];
	SectionB = new Shield*[a_sectionSize];
	SectionC = new Shield*[a_sectionSize];

	
	Generate(SectionA, a_sectionSize, 170, 100, a_Renderer,  a_Texture);
	Generate(SectionB, a_sectionSize, 590, 100, a_Renderer, a_Texture);
	Generate(SectionC, a_sectionSize, 1010, 100, a_Renderer, a_Texture);


}

ShieldGenerator::~ShieldGenerator()
{

}

void ShieldGenerator::Generate(Shield** a_array, float a_sectionSize, float a_x, float a_y, aie::Renderer2D* a_Renderer, aie::Texture* a_Texture)
{
	int X = 0;
	int Y = 0;
	for (int i = 0; i < a_sectionSize; i++)
	{
		Shield* TempShield = new Shield(a_x + 25 * X, a_y + 25 * Y, a_Renderer, a_Texture);
		a_array[i] = TempShield;
		X++;
		if (X == 4)
		{
			X = 0;
			Y++;
		}
	}
}

void ShieldGenerator::Draw()
{
	for (int i = 0; i < SectionSize; i++)
	{
		SectionA[i]->Draw();
		SectionB[i]->Draw();
		SectionC[i]->Draw();
	}
}