#pragma once
#include "Application.h"
#include "Renderer2D.h"
#include "Audio.h" 
#include "Application2D.h"
#include "Player.h"
#include "Texture.h"

class Bullet;
class EnemySpawn;
class EnemySpawn;
class Enemy : GameObject
{
public:
	Enemy () = delete;
	~Enemy() = default;
	
	Enemy(aie::Texture* a_texture, int a_width, int a_height, float a_x, float a_y, EnemySpawn* a_enemySpawn);
	

	void Update(float deltaTime) override;
	void SetSpeed(float speed) { m_speed = speed; }
	float GetSpeed() const { return m_speed; }

private:

	EnemySpawn* CurSpawner;

	bool HasFired = false;
	Bullet* bullet;
	void Shoot();
	float m_speed;

};
private:

};

Enemy : GameObject::Enemy : GameObject()
{
}

Enemy : GameObject::~Enemy : GameObject()
{
}
/*
class Enemy
{
public:
	Enemy();
	~Enemy();
	Enemy(float a_x, float a_y, EnemySpawn* Parent, float a_Width, float a_Height, Player* a_Player);

	void Draw(aie::Renderer2D* a_2drenderer);
	void update(float DeltaTime);
	void Shoot();
	void invertSpeed();
	void UpdateBounds();

	EnemySpawn* parent = nullptr;

	float Width;
	float Height;

	bool isAlive;
	bool CanShoot;

	struct Position
	{
		float PosX;
		float PosY;
	};
	Position Curpos;
	struct Bounds
	{
	Position TopLeft;
	Position BottomRight;

	};
	Bounds m_Bounds;

private:

	aie::Texture* m_Texture = new aie::Texture("./textures/Doughnut.png");
	Player* TargetPlayer;
	Bullet* Shot;
	float shootTimer;

	float MoveSpeed;
	bool HasRebound;

	

};
*/
