#include "GameObjcet.h"
#include "Texture.h"
#include "Renderer2D.h"

GameObject::GameObject(aie::Texture* a_Texture, int a_width, int a_height)
{
	m_texture = a_Texture;
	m_width = a_width;
	m_height = a_height;
}

void GameObject::Render(aie::Renderer2D * renderer)
{
	renderer->drawSprite(m_texture, m_position.x, m_position.y, m_width, m_height);

}

void GameObject::UpdateBounds()
{
	m_bounds.TopLeft.x= m_position.x - (m_width * 0.5f);
	m_bounds.TopLeft.y= m_position.y + (m_height * 0.5f);
	  
	m_bounds.BottomRight.x = m_position.x + (m_width * 0.5f);
	m_bounds.BottomRight.y = m_position.y- (m_height * 0.5f);
}
