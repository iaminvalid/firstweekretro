#include "Application2D.h"
#include "Texture.h"
#include "Font.h"
#include "input.h"

Application2D::Application2D() {

}

Application2D::~Application2D() {

}

bool Application2D::startup() {
	
	m_2dRenderer = new aie::Renderer2D();

	m_texture = new aie::Texture("./textures/numbered_grid.tga");
	m_PortalTexture = new aie::Texture("./textures/Portal.png");

	m_font = new aie::Font("./font/consolas.ttf", 32);

	m_audio = new aie::Audio("./audio/powerup.wav");

	m_cameraX = 0;
	m_cameraY = 0;
	m_timer = 0;

	EnemySetter = new EnemySpawn(4, 4, 200, 400);

	EnemySetter->SpawnEnemy(50, 40, 40, CurrPlayer);
	CurrPlayer = new Player(EnemySetter);
	shields = new ShieldGenerator(16, m_2dRenderer, m_PortalTexture);

	return true;
}

void Application2D::shutdown() {
	
	delete m_audio;
	delete m_font;
	delete m_texture;
	delete m_PortalTexture;
	delete m_2dRenderer;
}

void Application2D::update(float deltaTime) {

	m_timer += deltaTime;
	EnemySetter->Update(deltaTime);
	// input example
	aie::Input* input = aie::Input::getInstance();

	CurrPlayer->update(deltaTime,input);
	
	//// example of audio
	//if (input->wasKeyPressed(aie::INPUT_KEY_SPACE))
	//	m_audio->play();

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE) || EnemySetter->HasInvaded || CurrPlayer->IsAlive == false)
		quit();
}

void Application2D::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// set the camera position before we begin rendering
	m_2dRenderer->setCameraPos(m_cameraX, m_cameraY);
	// begin drawing sprites
	m_2dRenderer->begin();
	EnemySetter->Draw(m_2dRenderer);
	shields->Draw();

	//draw Player
	CurrPlayer->Draw(m_PortalTexture, m_2dRenderer);


	// output some text, uses the last used colour
	char score[32];
	char fps[32];
	sprintf_s(score, 32, "Score: %i", CurrPlayer->Score);
	sprintf_s(fps, 32, "FPS: %i", getFPS());

	m_2dRenderer->drawText(m_font, score, 40, 700);
	m_2dRenderer->drawText(m_font, fps, 0, 720 - 32);
	
	// done drawing sprites
	m_2dRenderer->end();
}
