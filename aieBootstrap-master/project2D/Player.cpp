#include "Player.h"
#include "Bullet.h"
#include "input.h"
#include "EnemySpawner.h"

//OLD PLAYER BEFORE INHERIT
/*

//move functions
void Player::MoveLeft()
{
	PlayerPos.Xpos -= 2.5;
};

void Player::MoveRight()
{
	PlayerPos.Xpos += 2.5;
};


//shoot function
void Player::Shoot()
{
	if (!BulletFired)
	{
	CurBullet = new Bullet(this, CurSpawner);

	}
	BulletFired = true;
};

void Player::Draw(aie::Texture* a_texture, aie::Renderer2D* a_renderer2D)
{
	float X = PlayerPos.Xpos;
	float Y = PlayerPos.Ypos;

	a_renderer2D->drawSprite(a_texture, X, Y, Width, Height);
	if (BulletFired && CurBullet->HasHit == false)
	{
		CurBullet->Draw(a_renderer2D);
	}
};

void Player::updateBounds()
{

	m_Bounds.TopLeft.Xpos = PlayerPos.Xpos - (Width * 0.5f);
	m_Bounds.TopLeft.Ypos = PlayerPos.Ypos + (Height * 0.5f);

	m_Bounds.BottomRight.Xpos = PlayerPos.Xpos + (Width * 0.5f);
	m_Bounds.BottomRight.Ypos = PlayerPos.Ypos - (Height * 0.5f);
}

void Player::update(float deltaTime, aie::Input* input)
{

	if (input->isKeyDown(aie::INPUT_KEY_LEFT))
		MoveLeft();

	if (input->isKeyDown(aie::INPUT_KEY_RIGHT))
		MoveRight();

	if (input->isKeyDown(aie::INPUT_KEY_SPACE))
	{
		Shoot();
	}
	if (BulletFired)
	{
		CurBullet->Update(deltaTime);
		if (CurBullet->HasHit)
		{
			delete CurBullet;
		}
		
	}
	updateBounds();
}*/



Player::Player(aie::Texture * a_texture, int a_width, int a_height, float a_x, float a_y, EnemySpawn* a_enemySpawn) : GameObject(a_texture, a_width, a_height)
{
	m_speed = 100;
	m_position.x = a_x;
	m_position.y = a_y;
	m_position.z = 1;
	CurSpawner = a_enemySpawn;
}

void Player::Update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_KEY_RIGHT || aie::INPUT_KEY_D))
	{
		m_position.x += m_speed * deltaTime;
	}

	if (input->isKeyDown(aie::INPUT_KEY_LEFT || aie::INPUT_KEY_A))
	{
		m_position.x -= m_speed * deltaTime;
	}

	if (input->isKeyDown(aie::INPUT_KEY_SPACE))
	{
		Shoot();
	}

}

void Player::Shoot()
{
	if (!HasFired)
	{
		bullet = new Bullet(this, CurSpawner);

	}
	HasFired = true;
}




