#include "Collider.h"
#include "Enemy.h"
#include "Player.h"
#include "Bullet.h"
Collider::Collider()
{
}

Collider::Collider(Position a_TopLeft, Position a_BottomRight)
{
	TopLeft = a_TopLeft;
	BottomRight = a_BottomRight;
}

Collider::~Collider()
{
}

bool Collider::IsEnemyColliding( Enemy* a_enemy, Bullet * a_bullet)
{
if (a_enemy->m_Bounds.TopLeft.PosX > a_bullet->m_Bounds.BottomRight.Xpos) return false;
if (a_enemy->m_Bounds.BottomRight.PosX < a_bullet->m_Bounds.TopLeft.Xpos) return false;
if (a_enemy->m_Bounds.TopLeft.PosY < a_bullet->m_Bounds.BottomRight.Ypos) return false;
if (a_enemy->m_Bounds.BottomRight.PosY > a_bullet->m_Bounds.TopLeft.Ypos) return false;

	return true;
}

bool Collider::IsPlayerColliding(Player * a_player, Bullet * a_bullet)
{
	if (a_player->m_Bounds.TopLeft.Xpos > a_bullet->m_Bounds.BottomRight.Xpos) return false;
	if (a_player->m_Bounds.BottomRight.Xpos< a_bullet->m_Bounds.TopLeft.Xpos) return false;
	if (a_player->m_Bounds.TopLeft.Ypos < a_bullet->m_Bounds.BottomRight.Ypos) return false;
	if (a_player->m_Bounds.BottomRight.Ypos > a_bullet->m_Bounds.TopLeft.Ypos) return false;
	
	return true;
}
