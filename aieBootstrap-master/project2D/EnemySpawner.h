#pragma once
# include "Renderer2D.h"

class Player;
class Enemy;

class EnemySpawn
{	
public:

	EnemySpawn(int a_Xsize, int a_Ysize, int a_startX, int a_startY);
	~EnemySpawn();
	void SpawnEnemy(float Spacing, float a_Width, float a_Height, Player* a_Player);
	void Update(float deltaTime);
	void Draw(aie::Renderer2D* a_renderer2D);
	void HasBounced();
	bool CanBounce();
	
	int total;

	bool Bounced;
	bool HasInvaded;

	struct Position
	{
		float x;
		float y;
	};
	Position CurPos;

	Enemy** TotalEnemies;

private:

	int CurCol;
	int RowSize;
	int ColSize;
	int CurSpawned;
	int TotalSpawned;
	
	float bouncetimer;
	Position SpawnPos;
	

};
