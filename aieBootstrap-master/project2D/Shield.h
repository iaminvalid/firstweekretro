#pragma once
#include "Renderer2D.h"

class Shield
{
public:
	struct Size
	{
		float width;
		float Height;
	};
	struct Position
	{
		float Xpos;
		float Ypos;
	};
	Position Position;
	Size Size;
	aie::Renderer2D* m_Renderer;
	aie::Texture* m_Texture;

	Shield();
	Shield(float X, float Y, aie::Renderer2D* a_Renderer, aie::Texture* a_Texture);
	~Shield();

	void Draw();
};
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

class ShieldGenerator
{
public:
	int SectionSize;

		Shield** SectionA;
		Shield** SectionB;
		Shield** SectionC;
	

	ShieldGenerator();
	ShieldGenerator(int a_sectionSize, aie::Renderer2D* a_Renderer, aie::Texture* a_Texture);
	~ShieldGenerator();

	void Draw();
	void Generate(Shield** a_array,float a_sectionSize, float a_x, float a_y, aie::Renderer2D* a_Renderer, aie::Texture* a_Texture);
};