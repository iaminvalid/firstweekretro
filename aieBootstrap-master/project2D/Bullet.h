#pragma once
#include "Player.h"
#include "Enemy.h"
#include "EnemySpawner.h"
#include "Texture.h"
# include "Renderer2D.h"
#include "Collider.h"
class Bullet {
public:
	
	Collider BulletCollider;

	Bullet();
	Bullet(Player* parentIsPlayer, EnemySpawn* enemies);
	Bullet(Enemy* parentIsEnemy, Player* PlayerTarget);
	~Bullet();
	void updateBounds();
	void Update(float deltatime);
	void Draw(aie::Renderer2D* a_renderer2D);

	aie::Texture* m_AmmoTexture;
	struct Position
	{
		float Xpos = 0.0f;
		float Ypos = 0.0f;
	};

	//direction of movement

	bool HasHit = false;

	float direction = 0;
	float Width  = 50;
	float Height = 50;
	aie::Texture* BulletTexture;
	aie::Texture* PBullet = new aie::Texture("./textures/HomerHead.png");
	aie::Texture* EBullet = new aie::Texture("./textures/HomerHead.png");

	//null if player is not parent
	EnemySpawn* Targets;
	Enemy* ParentE;

	//null if Enemy is not parent
	Player* ParentP;
	Player* Target;

	//position
	Position BulletPos;

	struct Bounds
	{
		Position TopLeft;
		Position BottomRight;

	};
	Bounds m_Bounds;

};