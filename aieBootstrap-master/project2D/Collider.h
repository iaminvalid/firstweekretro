#pragma once
class Enemy;
class Bullet;
class Player;
class Wall;
class Collider
{
	struct Position
	{
		float x;
		float y;
	};
public:
	Collider();
	Collider(Position a_BottomLeft, Position a_TopRight);
	~Collider();
	bool IsEnemyColliding(Enemy* a_enemy, Bullet* a_bullet);
	bool IsPlayerColliding(Player* a_player, Bullet* a_bullet);
	bool IsWallHit(Wall* a_Wall, Bullet a_bullet);
	Position TopLeft;
	Position BottomRight;
private:

};
