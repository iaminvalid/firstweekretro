#include "EnemySpawner.h"
#include "Enemy.h"

EnemySpawn::EnemySpawn(int a_Xsize, int a_Ysize, int a_startX, int a_startY)
{
	CurPos.x = a_startX;
	CurPos.y = a_startY;

	SpawnPos.x = a_startX;
	SpawnPos.y = a_startY;

	total = (a_Xsize * a_Ysize);

	TotalSpawned = 0;
	CurSpawned = 1;

	Bounced = false;
	HasInvaded = false;
	
	RowSize = a_Xsize;
	TotalEnemies = new Enemy* [total];
}

EnemySpawn::~EnemySpawn()
{
}

void EnemySpawn::SpawnEnemy(float Spacing, float a_Width, float a_Height, Player* a_Player)
{
	

		int x = 0;
		int y = 0;
		while (TotalSpawned < total)
		{
			Enemy* TempEnemy = new Enemy(SpawnPos.x + Spacing*x, SpawnPos.y + Spacing*y, this, a_Width, a_Height, a_Player);
			TotalEnemies[TotalSpawned] = TempEnemy;
			TotalSpawned++;
			x++;
			if (x == 4)
			{
				x = 0;
				y++;
			}
		}
	
}

void EnemySpawn::Update(float deltaTime)
{
	if (bouncetimer > 0)
	bouncetimer -= deltaTime;
	
	CurCol = 0;
	if (Bounced)
	{
		Bounced = false;
	}
	for (int i = 0; i < total ; i++)
	{
			
			if (i < RowSize)
			{
					CurCol = i;
				while (CurCol < total)
				{

					if (TotalEnemies[CurCol] != nullptr)
					{
						TotalEnemies[CurCol]->CanShoot = true;
						break;
					}
					else
					{
						CurCol += RowSize;
					}
				}
			}
		if (TotalEnemies[i] != nullptr)
		{
		
			TotalEnemies[i]->update(deltaTime);
		
			if (TotalEnemies[i]->isAlive == false)
			{
				delete TotalEnemies[i];
				TotalEnemies[i] = nullptr;
				
			}
		}
	}
	if (Bounced)
	{
		if (bouncetimer <=0)
		{

			for (int i = 0; i < total ; i++)

			{

				if (TotalEnemies[i] != nullptr)
				{
					TotalEnemies[i]->invertSpeed();
				}
			}
			bouncetimer = 1;
		}
	}

}

void EnemySpawn::Draw(aie::Renderer2D* a_renderer2D )
{
	for (int i = 0; i < total ; i++)
	{
		if (TotalEnemies[i] != nullptr)
		{
		TotalEnemies[i]->Draw(a_renderer2D);

		}
	}
	
}

void EnemySpawn::HasBounced()
{
	Bounced = true;
}

bool EnemySpawn::CanBounce()
{
	return Bounced;
}




