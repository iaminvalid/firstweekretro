#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "Audio.h"
#include "Enemy.h"
#include "EnemySpawner.h"
#include "player.h"
#include "Shield.h"

class Application2D : public aie::Application {
public:
	Player* CurrPlayer;

	Application2D();
	virtual ~Application2D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();


protected:

	ShieldGenerator* shields;
	EnemySpawn* EnemySetter;
	aie::Renderer2D*	m_2dRenderer;
	aie::Texture*		m_texture;
	aie::Texture*		m_PortalTexture;
	aie::Texture*		m_EnemyTexture;
	aie::Texture*		m_PAmmoTexture;
	aie::Texture*		m_EAmmoTexture;
	aie::Font*			m_font;
	aie::Audio*			m_audio;

	float m_cameraX, m_cameraY;
	float m_timer;
};