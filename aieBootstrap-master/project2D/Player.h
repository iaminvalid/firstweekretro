#pragma once
#include "Texture.h"
# include "Renderer2D.h"
#include "EnemySpawner.h"
#include "input.h"
#include "GameObjcet.h"
class Bullet;
class Player : GameObject
{
	
public:
	Player() = delete;
	Player(aie::Texture* a_texture, int a_width, int a_height, float a_x, float a_y, EnemySpawn* a_enemySpawn);
	~Player() = default;

	void Update(float deltaTime) override;
	void SetSpeed(float speed) { m_speed = speed; }
	float GetSpeed() const { return m_speed; }

private:

	EnemySpawn* CurSpawner;

	bool HasFired = false;
	Bullet* bullet;
	void Shoot();
	float m_speed;

};
//OLD BEFORE INHERIT
/*class Player {

	public:
		EnemySpawn* CurSpawner;
		void updateBounds();
		void update(float deltaTime, aie::Input* input);
	//position
		struct Position
		{
			float Xpos = 640.0f;
			float Ypos = 50.0f;
		}; 
		struct Bounds
		{
			Position TopLeft;
			Position BottomRight;

		};

		Bounds m_Bounds;
		Position PlayerPos;
	
		float Width = 100.0f;
		float Height = 50.0f;
		unsigned int Score = 0;
	//number of bullets
	bool BulletFired = false;
	bool IsAlive = true;
	Bullet* CurBullet;
	//constructor
	Player::Player(EnemySpawn* a_enemySpawn)
	{
		Score = 0;
		CurSpawner = a_enemySpawn;

	}
	//destructor
	Player::~Player() 
	{

	}


	//move functions
	void MoveLeft();

	void MoveRight();

	//shoot function
	void Shoot();

	void Draw(aie::Texture* a_texture, aie::Renderer2D* a_renderer2D);





};*/